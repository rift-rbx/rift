local tween = game:GetService("TweenService")

-- TODO maybe create these things in code?
local blur_effect = game.Lighting:WaitForChild("Blur")
local cc_effect = game.Lighting:WaitForChild("ColorCorrection")
local camera = game.Workspace.CurrentCamera

local tween_style = TweenInfo.new(0.5, Enum.EasingStyle.Quart, Enum.EasingDirection.Out)

local tween_in_blur = tween:Create(blur_effect, tween_style, {Size = 32})
local tween_out_blur = tween:Create(blur_effect, tween_style, {Size = 0})

local tween_in_fov = tween:Create(camera, tween_style, {FieldOfView = camera.FieldOfView - 15})
local tween_out_fov = tween:Create(camera, tween_style, {FieldOfView = camera.FieldOfView})

local tween_in_cc = tween:Create(cc_effect, tween_style, {Brightness = -0.1, Contrast = -0.2, Saturation = -0.2})
local tween_out_cc = tween:Create(cc_effect, tween_style, {Brightness = 0, Contrast = 0, Saturation = 0})

return {
    enter = function()
        tween_in_blur:Play()
        tween_in_fov:Play()
        tween_in_cc:Play()
    end,
    exit = function()
        tween_out_blur:Play()
        tween_out_fov:Play()
        tween_out_cc:Play()
    end,
}
local import = function(p) local r=script.Parent for k in string.gmatch(p,"[^/]+") do if k==".." then r=r.Parent else r=r:WaitForChild(k) end end return require(r) end

local set = import "../../Util/Set"

local mod = {}

function mod.new()
    local controller = {}

    controller.activated_menu = nil
    controller.deactivated_menus = set.new()

    controller.hook = nil

    function controller:register_active(m)
        if self.activated_menu then
            self.activated_menu.Visible = false
            set.insert(self.deactivated_menus, self.activated_menu)
        end

        m.Visible = true
        self.activated_menu = m
    end

    function controller:register(m)
        m.Visible = false
        set.insert(self.deactivated_menus, m)
    end

    function controller:set_hook(f)
        self.hook = f
    end

    function controller:activate(m)
        if self.activated_menu == m then return end

        if self.deactivated_menus[m] then
            -- deactivate current menu
            self.activated_menu.Visible = false
            set.insert(self.deactivated_menus, self.activated_menu)
            
            -- remove new active from deactivated
            set.remove(self.deactivated_menus, m)

            -- set new active
            m.Visible = true
            self.activated_menu = m
        end

        if self.hook then self.hook(m) end
    end

    function controller:connect(button, menu)
        -- TODO non-mouse things
        button.MouseButton1Click:connect(function()
            self:activate(menu)
        end)
    end

    return controller
end

return mod
local import = function(p) local r=script.Parent for k in string.gmatch(p,"[^/]+") do if k==".." then r=r.Parent else r=r:WaitForChild(k) end end return require(r) end

local I = import "../../Util/Declarative"

local mod = {}

function mod.xl_yc()
    return I.UIListLayout(
        { SortOrder = Enum.SortOrder.LayoutOrder
        , VerticalAlignment = Enum.VerticalAlignment.Center
        }, {}, {} )
end

return mod
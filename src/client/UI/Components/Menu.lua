local import = function(p) local r=script.Parent for k in string.gmatch(p,"[^/]+") do if k==".." then r=r.Parent else r=r:WaitForChild(k) end end return require(r) end

local tween = game:GetService("TweenService")
local guiservice = game:GetService("GuiService")

local I     = import "../../Util/Declarative"
local sound = import "../../Util/PlaySound"

local mod = {}

function mod.title(text)
    return I.TextLabel(
        { Size = UDim2.new(1, 0, 0, 80)
        , BackgroundTransparency = 1
        , BorderSizePixel = 0
        , Font = Enum.Font.Highway
        , Text = text
        , TextColor3 = Color3.new(1, 1, 1)
        , TextSize = 100
        , TextXAlignment = Enum.TextXAlignment.Left
        , TextYAlignment = Enum.TextYAlignment.Center
        }, {}, {} )
end

function mod.subtitle(text)
    return I.TextLabel(
        { Size = UDim2.new(1, 0, 0, 30)
        , BackgroundTransparency = 1
        , BorderSizePixel = 0
        , Font = Enum.Font.GothamBold
        , Text = text
        , TextColor3 = Color3.new(1, 1, 1)
        , TextSize = 32
        , TextTransparency = 0.5
        , TextXAlignment = Enum.TextXAlignment.Left
        , TextYAlignment = Enum.TextYAlignment.Center
        }, {}, {} )
end

function mod.spacer()
    return I.Frame(
        { Size = UDim2.new(1, 0, 0, 40)
        , BackgroundTransparency = 1
        , BorderSizePixel = 0
        }, {}, {} )
end

function mod.button(name, text)
    tween_normal = TweenInfo.new(0.2, Enum.EasingStyle.Quad, Enum.EasingDirection.Out)
    tween_quick = TweenInfo.new(0.1, Enum.EasingStyle.Quad, Enum.EasingDirection.Out)

    local text_rest
    local image_rest

    local text_hover
    local image_hover

    local image_click

    local function hover(self)
        text_hover:Play()
        image_hover:Play()
        sound("rbxassetid://140910211", 0.05)
    end

    local function leave(self)
        text_rest:Play()
        image_rest:Play()
    end

    local function click(self)
        image_click:Play()
        sound("rbxassetid://537744814", 0.1)
    end

    local last_click_time = 0 -- WTF

    local button = I.TextButton(
        { Name = name
        , Size = UDim2.new(0, 500, 0, 50)
        , BackgroundTransparency = 1
        , BorderSizePixel = 1
        , AutoButtonColor = false
        , Modal = true
        , Font = Enum.Font.Highway
        , Text = text
        , TextColor3 = Color3.new(1, 1, 1)
        , TextSize = 40
        , TextTransparency = 0.2
        , TextXAlignment = Enum.TextXAlignment.Left
        , TextYAlignment = Enum.TextYAlignment.Center
        },
        { InputBegan = function(self, input) -- TODO: touch support is still iffy
            if input.UserInputType == Enum.UserInputType.MouseMovement
                or input.UserInputType == Enum.UserInputType.Touch
            then
                guiservice.SelectedObject = self
            elseif input.UserInputType == Enum.UserInputType.MouseButton1
                or input.KeyCode == Enum.KeyCode.Return
                or input.KeyCode == Enum.KeyCode.ButtonA
            then
                if time() ~= last_click_time then
                    click(self)
                    last_click_time = time()
                end
            end
          end
        , InputEnded = function(self, input)
            if input.UserInputType == Enum.UserInputType.MouseMovement
                or input.UserInputType == Enum.UserInputType.Touch
            then
                if guiservice.SelectedObject == self then
                    guiservice.SelectedObject = nil
                end
            end
          end
        , SelectionGained = hover 
        , SelectionLost = leave
        , TouchTap = function(self) sound("rbxassetid://537744814", 0.1) end
        },
        { I.ImageLabel(
            { Name = "MenuButtonHover"
            , Size = UDim2.new(1, 0, 1, 0)
            , Position = UDim2.new(0, -25, 0, 0)
            , BackgroundTransparency = 1
            , BorderSizePixel = 1
            , Image = "rbxassetid://2685720352"
            , ImageTransparency = 1
            , ZIndex = -1
            }, {}, {} ) 
        } )

    text_rest = tween:Create(button, tween_normal, {TextTransparency = 0.2})
    image_rest = tween:Create(button.MenuButtonHover, tween_normal, {ImageTransparency = 1})

    text_hover = tween:Create(button, tween_normal, {TextTransparency = 0})
    image_hover = tween:Create(button.MenuButtonHover, tween_normal, {ImageTransparency = 0.8})

    image_click = tween:Create(button.MenuButtonHover, tween_quick, {ImageTransparency = 0.3})

    return button
end

return mod
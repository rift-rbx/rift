local import = function(p) local r=script.Parent for k in string.gmatch(p,"[^/]+") do if k==".." then r=r.Parent else r=r:WaitForChild(k) end end return require(r) end

local players = game:GetService("Players")
local guiservice = game:GetService("GuiService")
local cas = game:GetService("ContextActionService")
local inputservice = game:GetService("UserInputService")

local I = import "Util/Declarative"

local menu  = import "UI/Components/Menu"
local align = import "UI/Components/Align"

local blur = import "Effects/Blur"

local menu_controller = import "UI/Controllers/Menu"

print("[mainmenu] building menus")
local gui = I.ScreenGui({}, {},
    { I.Frame(
        { Name = "MainMenu"
        , Position = UDim2.new(0.1, 0, 0, 0)
        , Size = UDim2.new(0.9, 0, 1, 0)
        , BackgroundTransparency = 1
        , BorderSizePixel = 0
        }, {},
        { align.xl_yc()
        , menu.title("RIFT")
        , menu.subtitle("v0.1.0-dev")
        , menu.spacer()
        , menu.button("PlayButton", "PLAY")
        , menu.button("CollectionButton", "COLLECTION")
        , menu.button("OptionsButton", "OPTIONS")
        } )
    , I.Frame(
        { Name = "TestMenu"
        , Position = UDim2.new(0.1, 0, 0, 0)
        , Size = UDim2.new(0.9, 0, 1, 0)
        , BackgroundTransparency = 1
        , BorderSizePixel = 0
        }, {},
        { align.xl_yc()
        , menu.title("OWO")
        , menu.spacer()
        , menu.button("UwUButton", "UWU")
        , menu.button("BackButton", "BACK")
        } )
    , I.Frame(
        { Name = "TestMenu2"
        , Position = UDim2.new(0.1, 0, 0, 0)
        , Size = UDim2.new(0.9, 0, 1, 0)
        , BackgroundTransparency = 1
        , BorderSizePixel = 0
        }, {},
        { align.xl_yc()
        , menu.title("UWU")
        , menu.spacer()
        , menu.button("OwOButton", "OWO")
        , menu.button("BackButton", "BACK")
        } )
    }
)

print("[mainmenu] setting up menu controller")
local c = menu_controller.new()

c:register_active(gui.MainMenu)
c:register(gui.TestMenu)
c:register(gui.TestMenu2)

c:connect(gui.MainMenu.PlayButton, gui.TestMenu)
c:connect(gui.MainMenu.CollectionButton, gui.TestMenu)
c:connect(gui.MainMenu.OptionsButton, gui.TestMenu)

c:connect(gui.TestMenu.UwUButton, gui.TestMenu2)
c:connect(gui.TestMenu.BackButton, gui.MainMenu)

c:connect(gui.TestMenu2.OwOButton, gui.TestMenu)
c:connect(gui.TestMenu2.BackButton, gui.MainMenu)

c:set_hook(function(m)
    if m == gui.MainMenu then
        blur.exit()
    else
        blur.enter()
    end
end)

print("[mainmenu] setting up keyboard input")

print("[mainmenu] finishing up")
gui.Parent = players.LocalPlayer:WaitForChild("PlayerGui")

local function auto_select_menu_item()
    for _, v in next, c.activated_menu:GetDescendants() do
        if v:IsA("GuiObject") and v.Selectable then
            guiservice.SelectedObject = v
            break
        end
    end
end

cas:BindAction(
    "AutoSelect", function(_name, state, _input)
        if guiservice.SelectedObject ~= nil
            or state ~= Enum.UserInputState.Begin
        then
            return Enum.ContextActionResult.Pass
        end
        auto_select_menu_item()
    end, false,
    Enum.KeyCode.W, Enum.KeyCode.A, Enum.KeyCode.S, Enum.KeyCode.D,
    Enum.KeyCode.Up, Enum.KeyCode.Down, Enum.KeyCode.Left, Enum.KeyCode.Right,
    Enum.KeyCode.DPadUp, Enum.KeyCode.DPadDown, Enum.KeyCode.DPadLeft, Enum.KeyCode.DPadRight
)

local thumbstick_pos = Vector3.new(0, 0, 0)
inputservice.InputChanged:connect(function(input, _)
    if input.KeyCode == Enum.KeyCode.Thumbstick1 then
        thumbstick_pos = thumbstick_pos + input.Delta

        if guiservice.SelectedObject ~= nil
            or thumbstick_pos.Magnitude < 0.6 -- magic number, probably breaks on other controllers
        then
            return
        end

        auto_select_menu_item()
    end
end)

local _ = players.LocalPlayer or players.LocalPlayer.CharacterAdded:wait() -- wait for character
game.Workspace.CurrentCamera.CameraType = Enum.CameraType.Scriptable
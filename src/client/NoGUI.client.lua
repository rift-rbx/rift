local uis = game:GetService("UserInputService")
local sg = game:GetService("StarterGui")
local players = game:GetService("Players")

local pg = players.LocalPlayer:WaitForChild("PlayerGui")

print("[nogui] disabling coregui")
sg:SetCoreGuiEnabled(Enum.CoreGuiType.All, false)
pg.SelectionImageObject = Instance.new("ImageLabel")
pg:SetTopbarTransparency(1)

print("[nogui] disabling touch controls")
uis.ModalEnabled = true

print("[nogui] finished")
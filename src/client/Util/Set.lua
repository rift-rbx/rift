local mod = {}

function mod.insert(set, v)
    set[v] = v
end

function mod.remove(set, v)
    set[v] = nil
end

function mod.new(init)
    local set = {}

    for _, v in ipairs(init or {}) do
        mod.insert(set, v)
    end

    return set
end

return mod
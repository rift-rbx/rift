return function(id, ofs)
    local sound = Instance.new("Sound")
    sound.SoundId = id
    sound.PlayOnRemove = true
    sound.TimePosition = ofs or 0
    sound.Parent = game.Workspace
    sound:Destroy()
end
local mod = setmetatable({}, {
    __index = function(_, instanceType)
        return function(props, events, children)
            -- create instance
            local instance = Instance.new(instanceType)

            -- apply props
            for k, v in next, props do
                instance[k] = v
            end

            -- connect event handlers (adds self as first param)
            for k, v in next, events do
                instance[k]:connect(function(...) v(instance, ...) end)
            end

            -- parent children
            for _, v in ipairs(children) do
                v.Parent = instance
            end

            return instance
        end
    end
})

return mod